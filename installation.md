---
title: "Installation"
layout: page
---
# Installation

# Install as Part of KDE Software Compilation
Umbrello is part of KDE Software Compilation which means it comes with all GNU/Linux distributions.
You will be able to install it using your normal software installation app (Muon, Software Centre, Yast etc) or command line package manager (yum, up2date, yast, zypper, apt-get, emerge etc).<br>

The package will be called **umbrello**, some older distros will package it as **kdesdk**.<br>

---

## Fedora
At least on fedora 20 it is also required to install the package **oxygen-icon-theme**, otherwise umbrello will not show any icons.<br>

## FreeBSD
Umbrello is available from [FreeBSD ports](http://www.freebsd.org/ports/devel.html). <br>

## Linux as snap package
Umbrello is available on Linux as [snap package](https://snapcraft.io/umbrello).<br>

## Windows
Umbrello installer and portable packages for 32bit and 64 bit Windows are available at the [KDE download mirror network](http://download.kde.org/stable/umbrello/latest/).<br>

Umbrello on Windows is also part of the [KDE on Windows](http://windows.kde.org) distribution installed with the [KDE-Installer](http://download.kde.org/stable/kdewin/installer/kdewin-installer-gui-latest.exe.mirrorlist).<br>

Please note that the KDE on Windows distribution does not contain the latest umbrello releases.

## Mac OS X
Umbrello is available for Mac OS X on [MacPorts](https://www.macports.org/ports.php?by=name&substr=umbrello).

## Source Code on Linux like operating systems
The program has no platform specific code code, so should compile on any system with KDE.<br>

Here are the packages required for building Umbrello<br> 

 + [CMake](http://www.cmake.org)
 + GNU make
 + GNU C++ compiler (g++)
 + libxslt, libxml2
 + Qt5/KF5
 	+ Qt &gt;= 5.4, including development package.
    + KDE frameworks development package 5.x.
 + KDE4
   	+ Qt &gt;= 4.7, including development package.
    + kdelibs development package 4.8 or newer.
   

To install from source out of [git](http://git-scm.com), enter the following commands in a shell: <br>

```
mkdir -p $HOME/src
cd $HOME/src
git clone git://anongit.kde.org/umbrello  
cd umbrello
mkdir build
cd build
```

for building with Qt5/KF5 run:<br>

```
cmake -DCMAKE_INSTALL_PREFIX=$HOME/umbrello -DBUILD_KF5=1 -DCMAKE_BUILD_TYPE=Debug ../
```

and on building for KDE4<br>

```
cmake -DCMAKE_INSTALL_PREFIX=$HOME/umbrello -DBUILD_KF5=0 -DCMAKE_BUILD_TYPE=Debug ../
```

Check the cmake output for missing build requirements and install them<br>

```
make
make install
```

### Running the program from local installation
### Qt5/KF5 builds in a KF5 environment

```
... to be finished
```

#### Qt5/KF5 builds in a KDE4 environment

running umbrello KF5 build in a KDE4 environment requires to use the following commands.<br>

```
eval `dbus-launch`
kdeinit5
$HOME/umbrello/bin/umbrello &
```

#### under a KDE4 environment
Before running umbrello, you need set the KDEDIRS environment variable with:<br>

```
export KDEDIRS=$HOME/umbrello:$KDEDIRS
kbuildsycoca4
```
Run the following command from a shell:<br>
```
$HOME/umbrello/bin/umbrello &
```
You may add the following commands to your .bashrc or similar login shell config file to have umbrello in the users system path:<br>

```
export KDEDIRS=$HOME/umbrello:$KDEDIRS
export PATH=$HOME/umbrello/bin:$PATH
```

With that you can run umbrello simply by typing:<br>

```
umbrello
```

Note: There are reports that there may be no tool bar icons after installing umbrello/KDE4 in a non default location.<br>
If this happens to you, please install an official umbrello/KDE4 version with your package manager;
then start you just created umbrello.<br>

## Source Code on Windows

Umbrello could be build on windows from source with the [emerge build system](http://techbase.kde.org/Getting_Started/Build/Windows/emerge)<br>

## Cross compile Umbrello for Windows

Umbrello could be cross compiled for Windows from source with the help of the [openSUSE Build Service (OBS)](https://build.opensuse.org/) provided by the package [mingw32-umbrello](https://build.opensuse.org/package/show/windows%3Amingw%3Awin32/mingw32-umbrello) for 32bit builds and [mingw64-umbrello](https://build.opensuse.org/package/show/windows%3Amingw%3Awin64/mingw64-umbrello) for 64bit builds.<br>

Windows installer and portable packages are provided with [mingw32-umbrello-installer](https://build.opensuse.org/package/show/windows%3Amingw%3Awin32/mingw32-umbrello-installer) and [mingw64-umbrello-installer](https://build.opensuse.org/package/show/windows%3Amingw%3Awin64/mingw64-umbrello-installer).<br>

The mentioned obs packages provide remote builds by default. Local builds are possible on any Linux system with the help of the [OpenSuse command line build client](https://en.opensuse.org/openSUSE:OSC).<br>
    
And are useful to inspect and fix hidden issues happening with remote builds like non visible compiler or linker errors.
