<?php
	global $plasmaMenu;
	$plasmaMenu->addMenu("Project", "", "red.icon.png", "#ff96af");
	$plasmaMenu->addMenuEntry("Index", "/");
	$plasmaMenu->addMenuEntry("News", "news.php");
	$plasmaMenu->addMenuEntry("Screenshots","screenshots.php");
	$plasmaMenu->addMenuEntry("Features","features.php");
	$plasmaMenu->addMenuEntry("Documentation","documentation.md");
	$plasmaMenu->addMenuEntry("Installation","installation.php");
	$plasmaMenu->addMenuEntry("Press Articles","press.php");
	$plasmaMenu->addMenuEntry("FAQ","http://userbase.kde.org/Umbrello/FAQ");
	//$plasmaMenu->addMenuEntry("People","people.php");

	$plasmaMenu->addMenu("Support", "", "purple.icon.png", "#e285ff");
	//$plasmaMenu->addShortName("Umbrello Support");
        $plasmaMenu->addMenuEntry("IRC WebChat","http://webchat.freenode.net/?channels=umbrello&uio=MTE9MTk117");
        $plasmaMenu->addMenuEntry("IRC channel","irc://irc.freenode.net/umbrello");
	$plasmaMenu->addMenuEntry("Report a Bug", "https://bugs.kde.org/enter_bug.cgi?product=umbrello");
        $plasmaMenu->addMenuEntry("Mailing List","http://mail.kde.org/mailman/listinfo/umbrello");
	$plasmaMenu->addMenuEntry("Other Resources", "http://www.kde.org/support/");
	$plasmaMenu->addSubMenuEntry("International Sites", "http://www.kde.org/support/international.php");
	$plasmaMenu->addSubMenuEntry("Documentation", "http://docs.kde.org/");
	$plasmaMenu->addSubMenuEntry("Userbase Wiki", "http://userbase.kde.org/");
	$plasmaMenu->addSubMenuEntry("Sys Admin Wiki", "http://techbase.kde.org/SysAdmin");
	$plasmaMenu->addSubMenuEntry("Forums", "http://forum.kde.org/");
	$plasmaMenu->addSubMenuEntry("Mailing Lists", "http://www.kde.org/support/mailinglists/");
	$plasmaMenu->addSubMenuEntry("Security Advisories", "http://www.kde.org/info/security/");	
        $plasmaMenu->addSubMenuEntry("Join The Game", "http://jointhegame.kde.org/");	

        $plasmaMenu->addMenu("Community", "http://www.kde.org/community/", "green.icon.png", "#acff08");
	$plasmaMenu->addMenuEntry("About KDE", "http://www.kde.org/community/whatiskde/");
	$plasmaMenu->addSubMenuEntry("Software Compilation", "http://www.kde.org/community/whatiskde/softwarecompilation.php");
	$plasmaMenu->addSubMenuEntry("Project Management", "http://www.kde.org/community/whatiskde/management.php");
	$plasmaMenu->addSubMenuEntry("Development Model", "http://www.kde.org/community/whatiskde/devmodel.php");
	$plasmaMenu->addSubMenuEntry("Internationalization", "http://www.kde.org/community/whatiskde/i18n.php");
	$plasmaMenu->addSubMenuEntry("KDE e.V. Foundation", "http://ev.kde.org");
	$plasmaMenu->addSubMenuEntry("Free Qt Foundation", "http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php");
	$plasmaMenu->addSubMenuEntry("History", "http://www.kde.org/community/whatiskde/../history/");
	$plasmaMenu->addSubMenuEntry("Awards", "http://www.kde.org/community/whatiskde/../awards/");
	$plasmaMenu->addSubMenuEntry("Press Contact", "http://www.kde.org/community/whatiskde/../../contact/");
	$plasmaMenu->addMenuEntry("Announcements", "http://www.kde.org/announcements/");
	$plasmaMenu->addMenuEntry("Events", "http://events.kde.org/upcoming.php");
	$plasmaMenu->addMenuEntry("Get Involved", "http://www.kde.org/community/getinvolved/");
	$plasmaMenu->addMenuEntry("Donate", "http://www.kde.org/community/donations/");
	$plasmaMenu->addMenuEntry("Code Of Conduct", "http://www.kde.org/code-of-conduct/");
	$plasmaMenu->addMenuEntry("Press Page", "http://www.kde.org/presspage/");


	$plasmaMenu->addMenu("Developer Resources", "developers.php", "gray.icon.png", "#aaa");
	$plasmaMenu->addShortName("Developer Resources");
        $plasmaMenu->addMenuEntry("Developer Mailing List","http://mail.kde.org/mailman/listinfo/umbrello-devel");
	$plasmaMenu->addMenuEntry("Techbase Wiki", "http://techbase.kde.org/");
	$plasmaMenu->addMenuEntry("API Docs", "http://api.kde.org/");
	$plasmaMenu->addMenuEntry("Tutorials", "http://techbase.kde.org/Development/Tutorials/");
	
        
?>