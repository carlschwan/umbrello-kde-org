<?php
	include(dirname(__FILE__)."/lib.inc");
	$query = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	if (strpos($query,'/') !== FALSE)
		list($type, $version) = explode('/',$query);
	else {
		$type = $query;
		$version = "";
	}
	$url = buglist($type, $version);
	if ($url)
		Header("Location: $url");
?>