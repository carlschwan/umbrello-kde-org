---
title: 'KDE-Files.org Launched'
date: 2005-05-15 00:00:00 
layout: post
---

<p><a href="http://www.kde-files.org">KDE-Files.org</a> has launched today as a community website for sharing
templates and example files.  It incluedes a <a href="http://www.kde-files.org/index.php?xcontentmode=641">category for Umbrello
files</a>.  Please upload sample files and any UML diagrams you can spare
so others can learn from your UML techniques.</p>
