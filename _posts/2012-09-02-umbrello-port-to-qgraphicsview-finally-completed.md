---
title: 'Umbrello port to QGraphicsView finally completed'
date: 2012-09-02 00:00:00 
layout: post
---

<br />
Ralf today  <a href="https://projects.kde.org/projects/kde/kdesdk/umbrello/repository/revisions/1bf8a7204854cdff0e49ebff94a71bec5fd598ed">committed</a>
the QGraphicsScene and QGraphicsView changeover of UMLView and UMLScene.
This completes the bulk of the merge.  Cheers to Andi and Ralf for their untiring efforts!
