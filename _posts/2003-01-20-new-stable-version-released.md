---
title: 'New stable version released'
date: 2003-01-20 00:00:00 
layout: post
---

<p>Umbrello UML Modeller 1.1 has been released.</p>

<p>Currently only source and SuSE packages available.</p>

