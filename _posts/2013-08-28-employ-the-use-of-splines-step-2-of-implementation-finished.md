---
title: '"Employ the use of Splines" - Step 2 of Implementation finished'
date: 2013-08-28 00:00:00 
layout: post
---

<p>The implementation of <a href="https://bugs.kde.org/show_bug.cgi?id=107174">Bug 107174</a>, has been started several weeks ago. 
    Now thanks to the efforts of Andi Fischer and bug fixing by Ralf Habacker step 1 and 2 of the implementation plan has been finished and is merged to git master.</p>
