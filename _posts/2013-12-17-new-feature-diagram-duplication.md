---
title: 'New feature: diagram duplication'
date: 2013-12-17 00:00:00 
layout: post
---

<p>Joris Steyn added today a new diagram duplication feature.</p>
<p>This feature has been made possible after Joris fixed several bugs in the copy &amp; paste code and the undo implementation.</p>
<p>Many thanks for this contribution.</p>
