---
title: 'New - Users Page'
date: 2003-10-14 00:00:00 
layout: post
---

<p>It is the aim of Umbrello to encourage developers of Free (and
proprietry) Software to use UML for designing and documenting their
programmes.  Our new <a href="users.php">users page</a> lists some of the users of Umbrello.  
If you use or know of users who would like their projects
or organisations here please contact the <a href="contact.php">uml-devel</a> mailing list.</p>
