---
title: 'Umbrello 1.2 soon to be in beta'
date: 2003-10-14 00:00:00 
layout: post
---

<p>Umbrello is now in bugfixing mode along with the rest of KDE 3.2.  It should be stable enough to be
usable for real work, but obviously be cautious.  Umbrello again <a href="faq.php#kde31">compiles with KDE 3.1</a>
 thanks to Oliver.</p>
