---
title: 'UML 1.1 Beta 1 Released'
date: 2002-09-16 00:00:00 
layout: post
---

<p>Our first release in almost a year.  Updates include:</p>
<ul>
<li>A new XMI based file format</li>
<li>Activity and State Diagrams</li>
<li>Code import</li>
<li>Code export</li>
<li>Now uses QCanvas instead of QWidget for faster screen rendering</li>
<li>Many others</li>
</ul>
<p>Mind that this is a beta release and there's lots to be tidied up, 
but if you are having any problems with older releases or need any
of the new features you are advised to upgrade.</p>
