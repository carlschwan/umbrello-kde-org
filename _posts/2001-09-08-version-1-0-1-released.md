---
title: 'Version 1.0.1 Released'
date: 2001-09-08 00:00:00 
layout: post
---

<p>After non-stop bug fixing, Version 1.0.1 has been released.</p>
<p>Check out the ChangeLog for all the changes that have occured.</p>
<p>Classes now can have stereotypes and packages.</p>
<p>You can now select which diagrams you wish to print, with alot of printing
bug fixes implemented.</p> 
<p>Diagrams can now be larger by your requests.</p>
<p>With the changes made it is well worth the download if you like version 1.0.</p>
