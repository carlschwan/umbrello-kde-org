---
title: 'Sequence diagram enhancements'
date: 2014-01-05 00:00:00 
layout: post
---

<p>Ralf Habacker committed today enhancements for editing sequence diagram message properties:
<ul>
<li>The class operation combo box and custom operation input field has been enlarged to be able to see complete method names</li>
<li>The switch between class method and custom method selection has been automated to reduce required user interaction</li>
<li>It is now possible to directly add new operations, which reduces the required user interaction too</li>
</ul>
</p>
