---
title: 'Self Promotion'
date: 2001-09-21 00:00:00 
layout: post
---

<p>I thought I would take this opportunity with University finishing soon, to
promote myself. </p>
<p>I'm looking for a programming job or Post-Graduate position (with
scholarship) anywhere in Australia. </p>
<p>So if there is anyone who is interested could they E-Mail me and I can forward
the relevant information about myself. </p>
<p>Thanks,<br /><br />
Paul Hensgen <a href="mailto:phensgen@techie.com">phensgen@techie.com</a></p>
