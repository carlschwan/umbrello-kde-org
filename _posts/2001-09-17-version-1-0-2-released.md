---
title: 'Version 1.0.2 Released'
date: 2001-09-17 00:00:00 
layout: post
---

<p>With this version I consider that UML Modeller can start to be considered as a
good application. </p>
<p>Need to put your UML diagrams into a document or do you need to cut and paste
between diagrams? </p>
<p>You now can!  UML Modeller now supports Cut, Copy and Paste and multiple
selections.  Every Cut, Copy you make is put into the global clipboard as a PNG
picture to allow you to paste it into any program that supports PNG pictures.</p> 
<p>If that's not good enough, you can also paste onto other diagrams!</p>
<p>Version 1.0.2 also adds some features that you requested.  Class diagrams have
had the following associations added to them:  Implementations, Realizes,
Compositions and unidirectional associations.</p>
<p>Use Case diagrams have had Generalizations, unidirectional associations and
dependencies added. </p>
<p>Want to organise you Actors, Classes, and Use Cases?  You can now put them into
folders (Still in development). </p>
<p>Plus plenty of bugs have been fixed, check out the ChangeLog for more details.</p>
<p>NOTE: The cut and paste system is still new, so report those bugs to help make
this a better program for YOU!</p>
<p>With the new associations added, I have relaxed the rules on what the
association types can do, so let me know when you find a UML rule being broken.</p>
<p>Older files saved with UML Modeller will no longer load properly</p>
