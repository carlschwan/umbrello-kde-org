---
title: 'Improvements to Rational Rose model import'
date: 2014-08-20 00:00:00 
layout: post
---

<p>Oliver Kellog improved loading of Rational Rose models:
<ul>
<li>Support for importing controlled units</li>
<li>Support for importing class diagrams</li>
<li>Support for additional encodings:
<ul>
<li>Mac                     </li>
<li>ShiftJIS (Japanese)     </li>
<li>Hangul (Korean)         </li>
<li>Johab (Korean)          </li>
<li>GB2312 (Chinese)        </li>
<li>ChineseBig5             </li>
<li>Greek                   </li>
<li>Turkish                 </li>
<li>Vietnamese              </li>
<li>Hebrew                  </li>
<li>Arabic                  </li>
<li>Baltic                  </li>
<li>Russian                 </li>
<li>Thai                    </li>
<li>EastEurope              </li>
<li>OEM (extended ASCII)    </li>
</ul>
</li>
</ul>
Many thanks for this contribution.</p>
