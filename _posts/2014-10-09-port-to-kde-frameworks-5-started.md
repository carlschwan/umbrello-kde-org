---
title: 'Port to KDE frameworks 5 started'
date: 2014-10-09 00:00:00 
layout: post
---

<p>Today Andi Fisher started the port of umbrello to KDE frameworks 5. He expects to be ready in January 2015.</p>
