---
title: 'Suse/Debian Packages Available'
date: 2001-09-30 00:00:00 
layout: post
---

<p>Thanks to <a href="mailto:uml-webpage @ jriddell.org">Jonathan Riddell</a> RPMS for
Suse 7.1 are now available.</p>
<p>Also thanks go to <a href="mailto:wturkal@cbu.edu">Warren Turkal</a> for the Debian package</p>
