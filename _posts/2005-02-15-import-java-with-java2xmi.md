---
title: 'Import Java with Java2XMI'
date: 2005-02-15 00:00:00 
layout: post
---

<p>Robin Rawson-Tetley has written <a href="developers/java2xmi.tar.gz">Java 2 XMI</a>,
 a Java programme to import Java into Umbrello's XMI file format.</p>
