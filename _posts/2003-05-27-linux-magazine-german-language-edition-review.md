---
title: 'Linux Magazine (German language edition) Review'
date: 2003-05-27 00:00:00 
layout: post
---

<p>Fame at last, from <a href="http://www.linux-magazin.de/Artikel/ausgabe/2003/06/umbrello/umbrello.html">a review in Linux Magazine (in German)</a>.  <a href="http://translate.google.com/translate?u=http%3A%2F%2Fwww.linux-magazin.de%2FArtikel%2Fausgabe%2F2003%2F06%2Fumbrello%2Fumbrello.html&amp;langpair=de&amp;7Cen&amp;hl=en&amp;ie=ISO-8859-1&amp;prev=%2Flanguage_tools">Google translation to English</a>.</p>

<p>Umbrello was also featured on the subscription CD of the June 2003
English language edition of Linux Magazine.</p>

