---
title: 'Umbrello 1.1 release candidate 1'
date: 2002-10-23 00:00:00 
layout: post
---

<p>Featuring bug fixes and 101 improvements 1.1rc1 is now ready.</p>
