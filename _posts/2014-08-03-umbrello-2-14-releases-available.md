---
title: 'Umbrello 2.14 releases available'
date: 2014-08-03 00:00:00 
layout: post
---

<p>Umbrello 2.14 releases are available as part of the KDE Applications 4.14 releases. See <a href="/resolvedfeatures.php?4.14">new Umbrello 2.14 features</a> for a list of new features and <a href="/resolvedfeatures.php?4.14">Umbrello 2.14 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
