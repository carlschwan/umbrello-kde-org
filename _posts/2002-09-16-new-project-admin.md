---
title: 'New project admin'
date: 2002-09-16 00:00:00 
layout: post
---

<p><a href="http://jriddell.org/">Jonathan Riddell</a> has taken over as
the project administrator following the tragic spontanious combustion of 
Paul Hesgen's computer.  I'll be developing the programme as part of my
final year project at <a href="http://www.cs.stir.ac.uk">Stirling Uni</a> but 
others are more than welcome to help in the programme's development.</p>

