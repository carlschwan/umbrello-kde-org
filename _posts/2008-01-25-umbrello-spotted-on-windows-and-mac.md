---
title: 'Umbrello Spotted on Windows and Mac'
date: 2008-01-25 00:00:00 
layout: post
---

<a href="http://www.kde.org/kde-4.0-release-event/">KDE 4.0 Release Event</a> Umbrello was spotted running on both Windows and Mac OS X.  These ports of KDE apps are not yet stable but you can follow progress on <a href="http://windows.kde.org/">windows.kde.org</a> and <a href="http://techbase.kde.org/index.php?title=Projects/KDE_on_Mac_OS_X">KDE on Mac wiki page</a>.<br />
<img border="0" height="156" src="screenshots/umbrello2-windows-wee.jpg" style="float:left" width="250" />
See <a href="screenshots.php">the screenshots</a>.
