---
title: 'Beta 2 Released'
date: 2001-08-24 00:00:00 
layout: post
---

<p>Plenty of bug fixes.</p>
<p>More rules and correct error messages for associations implemented.</p>
<p>Default colour scheme now mimics Rational Rose.</p>
<p>Printing still to be implemented.  Expect this within two weeks!!!</p>
