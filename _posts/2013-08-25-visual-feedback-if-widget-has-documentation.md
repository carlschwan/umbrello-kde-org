---
title: 'Visual feedback if widget has documentation'
date: 2013-08-25 00:00:00 
layout: post
---

<p>
Ralf Habacker adds a graphical indicator to graphical widgets, which has documentation. This feature, which is useful to get a quick overview what is documentated and what not,
is enabled by an entry in the diagram context menu and if enabled, displays a triangle in the lower left corner of the related widget.</p>
