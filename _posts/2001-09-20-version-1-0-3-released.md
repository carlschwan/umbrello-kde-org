---
title: 'Version 1.0.3 Released'
date: 2001-09-20 00:00:00 
layout: post
---

<p>This release is for all the people who need to convert version 1.0.1 files to
the latest version. </p>
<p>It can load both 1.0.1 and 1.0.2 files, but due to a bug in the code of 1.0.2
there is a chance that in the list view (left hand side window), some items may
end up in folders that were not initially in folders. </p>
<p>Once loaded, I would suggest saving and reloading before using as this will make
sure that everything is correct. </p>
<p>The only other new features in this version are that text now remembers it
position and diagrams can now be put into folders. </p>
