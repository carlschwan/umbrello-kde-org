---
title: 'Umbrello 2.0 Release Starts Something Amazing'
date: 2008-01-12 00:00:00 
layout: post
---

<p>Umbrello 2.0 has been released as part of the kdesdk module in KDE 4.  Thanks to our contributors Oliver, Sharan, Andi and others for their work on this milestone.  Install it from your distro as part of kdesdk.</p>
