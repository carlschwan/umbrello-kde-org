---
title: 'Work stopped on Umbrello 2'
date: 2003-08-06 00:00:00 
layout: post
---

<p>Andrew Sutton has stopped work on Umbrello 2.  He will work on other areas of Umbrello.  Work continues on Umbrello 1.2 as part of KDE.</p>

