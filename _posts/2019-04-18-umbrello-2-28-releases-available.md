---
title: 'Umbrello 2.28 releases available'
date: 2019-04-18 00:00:00 
layout: post
---

<p>Umbrello 2.28 releases are available as part of the KDE Applications 19.04 releases. See <a href="/resolvedfeatures.php?19.04">new Umbrello 2.28 features</a> for a list of new features and <a href="/resolvedfeatures.php?19.04">Umbrello 2.28 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
<p>Binary packages are available from related Linux distributions and for 32bit and 64 bit Windows from the KDE <a class="piwik_link" href="http://download.kde.org/stable/umbrello/latest/">download mirror network</a>.</p>
