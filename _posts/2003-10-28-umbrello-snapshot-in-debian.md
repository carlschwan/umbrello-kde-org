---
title: 'Umbrello Snapshot in Debian'
date: 2003-10-28 00:00:00 
layout: post
---

<p>A recent shapshot of Umbrello from 2003-08-28 is now in Debian
unstable (sid).  This date was chosen for the stability of Umbrello at
that time.  <em>apt-get install umbrello</em> is now possible.  We
hope this will be in the next stable version of Debian (KDE 3.2 is unlikely to be).</p>
