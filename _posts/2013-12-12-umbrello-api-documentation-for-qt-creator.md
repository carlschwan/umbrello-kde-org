---
title: 'Umbrello api documentation for Qt Creator'
date: 2013-12-12 00:00:00 
layout: post
---

<p>Umbrello developers now can use umbrello api documentation inside Qt Creator.</p>
<p>Download <a href="umbrello-4.12.qch">umbrello-4.12.qch</a> and <a href="http://qt-project.org/doc/qtcreator-2.8/creator-help.html">install</a> it in Qt Creator.</p>
