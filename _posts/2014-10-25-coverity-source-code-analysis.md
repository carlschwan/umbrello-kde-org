---
title: 'Coverity source code analysis'
date: 2014-10-25 00:00:00 
layout: post
---

<p>Umbrello has been registered as coverity <a href="https://scan.coverity.com/projects/3327">project</a>, which helps to increase the code quality.</p>
<p>High impact issues from the source code scan has been already fixed by Oliver Kellog, for the remaining detects volunteers are welcome.
If you want to help to improve the code quality of umbrello, please contact <a href="mailto:okellogg@users.sourceforge.net">Oliver</a>.</p>
