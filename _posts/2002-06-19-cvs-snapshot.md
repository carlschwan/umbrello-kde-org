---
title: 'CVS Snapshot'
date: 2002-06-19 00:00:00 
layout: post
---

<p>A snapshot of CVS is now available for you to try.<br />
Lots of new features including State and Activity diagrams, Code importing and generation and many more.<br /><br />
UML Modeller now uses a XML format with XMI complience being worked on.<br /><br />
Report any bugs you find and request any features you want aswell.</p>
