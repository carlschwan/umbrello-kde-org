---
title: 'Beta 1 Released'
date: 2001-08-15 00:00:00 
layout: post
---

<p>Beta 1 of the program is ready for download.</p>
<p> The program may still have a few bugs, so report those bugs as you find them.</p>
<p> Printing is not implemented.  This will be implemented after KDE 2.2 is released.</p>
<p> The on-line help only opens the index page of the help at the moment and the
 help still needs to be refined and in some cases implemented.</p>
