---
title: 'Umbrello 1.2.1 Released'
date: 2004-03-15 00:00:00 
layout: post
---

<p>Umbrello UML Modeller 1.2.1 has been released with KDE 3.2.1.  This fixes several important issues with 1.2.  A
source package for KDE 3.1 is available.</p>
