---
title: ' Huge bug fixing '
date: 2012-12-31 00:00:00 
layout: post
---

<a href="https://bugs.kde.org/buglist.cgi?chfieldto=2012-12-31&amp;chfield=bug_status&amp;query_format=advanced&amp;chfieldfrom=2012-01-01&amp;chfieldvalue=RESOLVED&amp;product=umbrello">
fixed</a>. This let umbrello be run much more stable.
