---
title: 'Umbrello 2.18 releases available'
date: 2015-12-09 00:00:00 
layout: post
---

<p>Umbrello 2.18 releases are available as part of the KDE Applications 15.12 releases. See <a href="/resolvedfeatures.php?15.12">new Umbrello 2.18 features</a> for a list of new features and <a href="/resolvedfeatures.php?15.12">Umbrello 2.18 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
