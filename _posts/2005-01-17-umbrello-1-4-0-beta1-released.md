---
title: 'Umbrello 1.4.0-beta1 Released'
date: 2005-01-17 00:00:00 
layout: post
---

<p>Umbrello 1.4.0-beta1 has been released for testing along with KDE 3.4.0-beta1.</p>
