---
title: 'Support for zipped XML project file import (*.zargo extension)'
date: 2014-05-24 00:00:00 
layout: post
---

<p>Umbrello now supports importing the UML model from a zipped XML project file using the <b>*.zargo</b> file extension, 
which is used for example by <a href="http://argouml.tigris.org/">ArgoUML</a> and <a href="http://www.gentleware.com/new-poseidon-for-uml-8-0.html">Poseidon for UML</a>.</p>
<p>A zipped XML project file contains an UML model (in XMI format), diagrams and project informations.</p>
