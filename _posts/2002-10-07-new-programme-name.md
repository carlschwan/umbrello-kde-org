---
title: 'New Programme Name'
date: 2002-10-07 00:00:00 
layout: post
---

<p>Our programme name vote is now closed.  With 126 votes out of 271
the new name is <strong>Umbrello UML Modeller</strong>.</p>
