---
title: 'Umbrello 2.19 releases available'
date: 2016-04-20 00:00:00 
layout: post
---

<p>Umbrello 2.19 releases are available as part of the KDE Applications 16.04 releases. See <a href="/resolvedfeatures.php?16.04">new Umbrello 2.19 features</a> for a list of new features and <a href="/resolvedfeatures.php?16.04">Umbrello 2.19 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
