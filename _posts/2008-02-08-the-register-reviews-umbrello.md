---
title: 'The Register Reviews Umbrello'
date: 2008-02-08 00:00:00 
layout: post
---

<a href="http://www.regdeveloper.co.uk/2008/01/29/linux_uml_modeling/">The Register has reviewed some UML tools</a> and describes Umbrello as "<em>a nice, clean and simple UML editor</em>".
