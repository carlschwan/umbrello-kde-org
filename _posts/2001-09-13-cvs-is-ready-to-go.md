---
title: 'CVS is ready to go.'
date: 2001-09-13 00:00:00 
layout: post
---

<p><b>UPDATE:</b> Currently having trouble with CVS server.  I have let sourceforge
know and hopefully will be fixed soon.  Let me know when you can log in to
server. </p> <p>CVS is now available, so you can keep up to date with the latest
developments.</p>
<p>CVS currently has a few minor bug fixes included but also includes a new feature
- Cut, Copy and Paste. </p>
<p>You can now cut, copy and paste in between diagrams.  It also has built in rules
checking to not allow you to break UML rules.  It also puts a PNG picture of the
cut/copy into the global clipboard to allow you to paste into any program that
can handle PNG picture pastes.  </p>
