---
title: 'Umbrello 1.3 Released'
date: 2004-08-16 00:00:00 
layout: post
---

<p>Umbrello 1.3.0 has been released.  This version comes with KDE 3.3.</p>
<p>This release of Umbrello features improved memory management for
large diagrams, tools for aligning objects, compressed XMI file
support, new icons and better XMI standard conformance.</p>
