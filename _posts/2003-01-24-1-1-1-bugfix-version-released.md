---
title: '1.1.1 bugfix version released'
date: 2003-01-24 00:00:00 
layout: post
---

<p>1.1.1 fixes the bug which caused a crash on printing.  SuSE and Red Hat RPMs available.</p>
