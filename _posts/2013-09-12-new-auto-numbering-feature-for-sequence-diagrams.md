---
title: 'New auto numbering feature for sequence diagrams'
date: 2013-09-12 00:00:00 
layout: post
---

<p>Ralf Habacker committed today a new feature for sequence diagrams.</p>
<p>Sequence diagram messages could have sequence numbers to indicate the ordering. The dialog for message adding has now a new 'autoincrement' checkbox,
    with which an automatically increment of the sequence number could be enabled.</p>
