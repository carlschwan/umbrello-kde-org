---
title: 'Umbrello Quickies'
date: 2003-12-08 00:00:00 
layout: post
---

<p>Brazillian users can watch out for Umbrello in the KDE-special end of year edition of <a href="http://www.revistadolinux.com.br/">Revistado Linux Magazine</a> and again with a full article in the January edition.</p>

<p>Umbrello is also briefly featured in Chris Howell's desktop column in this months <a href="http://www.linuxformat.co.uk/">Linux Format magazine</a> published in the UK and other places.</p>

<p>Last month Umbrello developer Jonathan Riddell received runner up in <a href="http://www.scotlandis.com/">Scotland IS's</a> Young Software Engineer of the Year Awards 2003 for his <a href="http://jriddell.org/programs/umbrello/html/">dissertation on Umbrello</a> at the University of Stirling.  <a href="http://jriddell.org/photos/2003-11-ISScotland-awards-group.jpg">Photo, Jonathan second from right</a>.</p>

<p>Umbrello 1.2 beta has been released with KDE 3.2 beta (KDE 3.2
beta-2/3.1.94 has just been released but not yet announced).  All feedback
welcome.</p>
