---
title: ' Diagram auto layouts'
date: 2012-05-07 00:00:00 
layout: post
---

<p>Umbrello got a new diagram auto layout feature from Ralf Habacker. It uses graph generators from the 
<a href="http://www.graphviz.org/">graphviz</a> package which is included in many linux 
  distributions and is also be available on windows.</p>
<p>The auto layout feature is available through entries of a diagram context menu. 
For each diagram type there are multiple layout types available, which are customizable using configuration files. 
For an example see <a href="https://projects.kde.org/projects/kde/kdesdk/umbrello/repository/revisions/master/entry/umbrello/layouts/class-vertical.desktop">Vertical layout for class diagrams.</a></p>
