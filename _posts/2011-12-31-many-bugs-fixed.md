---
title: ' Many bugs fixed'
date: 2011-12-31 00:00:00 
layout: post
---

<a href="https://bugs.kde.org/buglist.cgi?chfieldto=2011-12-31&amp;chfield=bug_status&amp;query_format=advanced&amp;chfieldfrom=2011-01-01&amp;chfieldvalue=RESOLVED&amp;product=umbrello">
fixed</a>.
