---
title: 'First sign of life in KDE4'
date: 2006-07-28 00:00:00 
layout: post
---

<a href="https://projects.kde.org/projects/kde/kdesdk/umbrello/repository/show?rev=master">Umbrello</a> makes its debut on <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-06-26.0683224314/">Qt4</a> / <a href="http://www.answers.com/topic/kde4">KDE4</a> - but there is still <a href="http://www.geeksoc.org/~jr/umbrello/uml-devel/9648.html">a lot to do</a>.
