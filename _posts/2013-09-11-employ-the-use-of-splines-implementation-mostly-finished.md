---
title: '"Employ the use of Splines" - Implementation mostly finished'
date: 2013-09-11 00:00:00 
layout: post
---

<p>The implementation of <a href="https://bugs.kde.org/show_bug.cgi?id=107174">Bug 107174</a> is now mostly finished; the remaining bugs will be fixed in the next time.</p>
