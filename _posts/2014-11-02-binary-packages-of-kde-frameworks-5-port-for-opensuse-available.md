---
title: 'Binary packages of KDE frameworks 5 port for opensuse available'
date: 2014-11-02 00:00:00 
layout: post
---

<p>Today Wolfgang Bauer started to build <a href="http://software.opensuse.org/package/umbrello5">umbrello</a>
binary packages from the umbrello <a href="https://projects.kde.org/projects/kde/kdesdk/umbrello/repository/show?rev=frameworks">frameworks</a>
git branch for opensuse, which makes it possible to follow the porting process very easy.</p>
<p>Bug reports and/or patches are wery welcome and should be reported through the KDE <a href="https://bugs.kde.org/enter_bug.cgi?product=umbrello&amp;format=guided">bug tracker</a>.</p>
