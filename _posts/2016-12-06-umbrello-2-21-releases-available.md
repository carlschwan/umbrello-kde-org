---
title: 'Umbrello 2.21 releases available'
date: 2016-12-06 00:00:00 
layout: post
---

<p>Umbrello 2.21 releases are available as part of the KDE Applications 16.12 releases. See <a href="/resolvedfeatures.php?16.12">new Umbrello 2.21 features</a> for a list of new features and <a href="/resolvedfeatures.php?16.12">Umbrello 2.21 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
<p>Binary packages are available from related Linux distributions and for 32bit and 64 bit Windows from the KDE <a class="piwik_link" href="http://download.kde.org/stable/umbrello/latest/">download mirror network</a>.</p>
