---
title: 'Screenshots Wanted'
date: 2003-12-28 00:00:00 
layout: post
---

<p>Our <a href="screen.php">Screen Shots page</a> shows the now quite
old Umbrello 1.1.  With Umbrello 1.2 due to be released in January
we're now looking for screen shots of the new version.  If you are
using a 1.2-beta please take a capture of Umbrello doing something
interesting, put it on a webserver and e-mail to URL to umbrello @
jriddell.org</p>
