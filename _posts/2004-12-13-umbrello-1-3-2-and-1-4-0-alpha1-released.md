---
title: 'Umbrello 1.3.2 and 1.4.0-alpha1 Released'
date: 2004-12-13 00:00:00 
layout: post
---

<p>Umbrello 1.3.2 has been released along with KDE 3.3.2 and Umbrello 1.4.0-alpha1 has been released for testing along with KDE 3.4.0-alpha1.</p>
