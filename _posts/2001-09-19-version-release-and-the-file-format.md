---
title: 'Version release and the file format'
date: 2001-09-19 00:00:00 
layout: post
---

<p>I have had some negeative feedback (valid though) in regards to new releases not 
 supporting older file formats. </p>
<p>Some comments made have been that the releases have been minor and that the file
format should not change for such a minor release. </p>
<p>They have been minor releases, but alot has changed in the code.  This change in
code has occurred to allow future improvements in the program to be made easier
and thus meaning future features added quicker. </p>
<p>The file format used at the moment was never meant to be a permanent format.
The program will be changed for version 2.0 to use XMI, the standard for UML
diagrams. </p>
<p>I therefore need your opinion on the way the program should be released.  There
are two ways I'm looking at now. </p>
<p><b>1 -</b> Release one more 1.x version to iron out the bugs and only release
future 1.x versions as bug fixes and leave the file format static. </p>
<p>The developers would then start on the 2.0 branch which would not be released
for a couple of months.  This would use the XMI file format and also have code
to support the old file format of 1.x. </p>
<p>This would mean you would not get new features as quick unless you used the CVS
version.  But it would mean that the files you create would be able to be loaded
with any future versions. </p>
<p><b>2 -</b> The other way we can go is to keep going as we are, but this would
mean the file format would keep changing, but you would get new features
quicker. </p>
<p>I am leaning towards the first option, as you get the best of both worlds.  A
stable file format and if you want the new features you can use the CVS
version (which will be kept stable).</p>
<p>Please let me know what you think.</p>
