---
title: 'Umbrello Competes with Commercial Alternatives'
date: 2003-10-14 00:00:00 
layout: post
---

<p>Poseidon and at least one other company have bought Umbrello as AdWords on Google.  We would like
to thank these companies for acknowledging that our Free Software competes with their proprietary 
and commercial offerings.</p>
