---
title: 'MySQL and PostgreSQL schema import'
date: 2015-07-20 00:00:00 
layout: post
---

<p>Umbrello supports MySQL and PostgreSQL export since a long time. With the upcoming umbrello 2.17 release (KDE Applications 15.08) umbrello also gets support for importing MySQL and PostgreSQL schema,
    which enables round trip engineering. Many thanks to Ralf Habacker, who added this feature.</p>
