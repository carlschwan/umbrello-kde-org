---
title: 'Windows binary packages starting with umbrello version 2.17.0 available'
date: 2015-08-25 00:00:00 
layout: post
---

<p>Starting with umbrello version 2.17.0 binary packages of umbrello for 32bit and 64 bit Windows are available and could be downloaded from
 the KDE <a class="piwik_link" href="http://download.kde.org/stable/umbrello/latest/">download mirror network</a>.</p>
<p>The binaries has been build on the <a class="piwik_link" href="https://build.opensuse.org/package/show?project=windows%3Amingw%3Awin32&amp;package=mingw32-umbrello">OpenSUSE build service</a>
 using GNU cross compilers. Many thanks to Ralf Habacker, who organized the build system and provided the binary packages.</p>
