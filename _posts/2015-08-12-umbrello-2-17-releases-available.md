---
title: 'Umbrello 2.17 releases available'
date: 2015-08-12 00:00:00 
layout: post
---

<p>Umbrello 2.17 releases are available as part of the KDE Applications 15.08 releases. See <a href="/resolvedfeatures.php?15.08">new Umbrello 2.17 features</a> for a list of new features and <a href="/resolvedfeatures.php?15.08">Umbrello 2.17 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
