---
title: 'Umbrello now part of KDE'
date: 2003-01-24 00:00:00 
layout: post
---

<p>Umbrello 1.1.1 is now in KDE's CVS in kdesdk.  It will be released with KDE 3.2</p>
<p>Bugs and feature requests should now be reported to <a href="http://bugs.kde.org">http://bugs.kde.org</a>.</p>
