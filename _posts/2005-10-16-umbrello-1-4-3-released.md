---
title: 'Umbrello 1.4.3 Released'
date: 2005-10-16 00:00:00 
layout: post
---

<p>Umbrello 1.4.3 has been released along with KDE 3.4.3.  Packages are available as part of Kubuntu Breezy and other distributions.</p>
