---
title: 'Umbrello 1.1 Beta 2 Released'
date: 2002-10-16 00:00:00 
layout: post
---

<p>The biggest change is the move to the new name: <strong>Umbrello UML Modeller</strong>
<br />Other updates:</p>
<ul>
<li>Many bugfixes</li>
<li>Some nice graphical user interface (GUI) enhancements</li>
<li>Support for old file format</li>
<li>Hopefully last version before major release 1.1</li>
</ul><p>Please test this release and report all remaining bugs to the
<a href="http://sourceforge.net/tracker/?group_id=24919&amp;atid=382951">bug tracking system</a>
or contact the <a href="http://sourceforge.net/mail/?group_id=24919">mailing lists</a>.</p>
