---
title: 'Umbrello 2.16 releases available'
date: 2015-04-07 00:00:00 
layout: post
---

<p>Umbrello 2.16 releases are available as part of the KDE Applications 15.04 releases. See <a href="/resolvedfeatures.php?15.04">new Umbrello 2.16 features</a> for a list of new features and <a href="/resolvedfeatures.php?15.04">Umbrello 2.16 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
