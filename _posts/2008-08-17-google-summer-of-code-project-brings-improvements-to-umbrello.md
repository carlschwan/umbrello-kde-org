---
title: 'Google Summer of Code project brings improvements to Umbrello '
date: 2008-08-17 00:00:00 
layout: post
---

<a href="http://krishnaggk.blogspot.com/">Gopala Krishna</a> is <a href="http://cia.vc/stats/author/gopala/">implementing</a>
some <a href="http://krishnaggk.blogspot.com/2008/06/umbrello-gets-gradient-support.html">exciting</a> <a href="http://krishnaggk.blogspot.com/2008/07/refracturing-umbrello.html">
improvements</a> as part of his  <a href="http://www.google.com">Google</a> <a href="http://code.google.com/soc/2008/kde/about.html">
Summer of Code</a> project.<br />
His  work is currently happening in a <a href="https://projects.kde.org/projects/kde/kdesdk/umbrello/repository/show?rev=soc-umbrello">branch</a> but will soon be merged to the <a href="https://projects.kde.org/projects/kde/kdesdk/umbrello/repository/show?rev=master">trunk</a>. Stay tuned.
