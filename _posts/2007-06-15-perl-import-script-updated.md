---
title: 'Perl import script updated'
date: 2007-06-15 00:00:00 
layout: post
---

<a href="http://sourceforge.net/mailarchive/forum.php?thread_name=20070614103830.M553%40opensource.cl&amp;forum_name=uml-user">Hans Poo</a> contributed his <a href="developers/perl2xmi">perl2xmi</a> script, a standalone Perl program to import Perl into Umbrello's XMI file format. See <a href="developers.php">Developer's page</a>
