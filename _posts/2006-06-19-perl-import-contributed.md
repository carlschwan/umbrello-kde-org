---
title: 'Perl import contributed'
date: 2006-06-19 00:00:00 
layout: post
---

<a href="developers/xmi_reverse.pl">xmi_reverse.pl</a> is a standalone Perl program to import Perl into Umbrello's XMI file format. See <a href="developers.php">Developer's page</a>.
