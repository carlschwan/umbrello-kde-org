---
title: 'Umbrello 2.13 releases available'
date: 2014-03-30 00:00:00 
layout: post
---

<p>Umbrello 2.13 releases are available as part of the KDE Applications 4.13 releases. See <a href="/resolvedfeatures.php?4.13">new Umbrello 2.13 features</a> for a list of new features and <a href="/resolvedfeatures.php?4.13">Umbrello 2.13 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
