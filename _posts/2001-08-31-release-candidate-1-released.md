---
title: 'Release Candidate 1 Released'
date: 2001-08-31 00:00:00 
layout: post
---

<p>All features have been implemented for the version 1 release, including
printing.  We are now in feature freeze. </p>
<p>I have removed heaps of bugs and it's becoming harder to find them, so the final
version is not far away. </p>
<p>Please download and send those bugs and comments in.  I would really like to
know what you think of the program, so send me an e-mail and let me know.
  I would even like to hear the bad comments. </p>
