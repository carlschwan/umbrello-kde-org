---
title: 'Umbrello 1 and 2'
date: 2003-02-12 00:00:00 
layout: post
---

<p>The inital code for Umbrello 2 is now in <a href="http://sourceforge.net/cvs/?group_id=24919">Sourceforge's CVS</a> under the umbrello2 module.  This is a complete re-write and doesn't currently do anything.</p>
<p>Meanwhile development of Umbrello 1 continues apace in KDE's CVS.</p>
