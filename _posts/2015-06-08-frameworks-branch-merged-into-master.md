---
title: 'Frameworks branch merged into master'
date: 2015-06-08 00:00:00 
layout: post
---

<p>In the last weeks the port to KDE frameworks 5 has been merged back into the master branch. Many thanks to Andi Fischer, who ported umbrello to KF5
and Ralf Habacker, who merged back the changes into the master branch.</p>
<p>The merge back has been designed to support KDE4 and KF5 builds from the same source. To create a KF5/Qt5 based build -DBUILD_KF5=1 has to be added
    to the cmake command line.</p>
