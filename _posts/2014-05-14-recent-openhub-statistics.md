---
title: 'Recent OpenHub Statistics'
date: 2014-05-14 00:00:00 
layout: post
---

<p>
Over the past twelve months, 18 developers contributed to Umbrello UML Modeller. This project has a relatively large team, in the top 10% of all project teams on OpenHub.
 For this measurement, OpenHub considers only recent changes to the code. Over the entire history of the project, 115 developers have contributed. See the full story <a href="https://www.openhub.net/p/umbrello?ref=sample">here</a>.
</p>
