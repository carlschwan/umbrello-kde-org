---
title: 'Developers required'
date: 2001-09-19 00:00:00 
layout: post
---

<p>My University has given me permission to take developers on board.  If you are
interested in being a developer for this project, E-Mail me with details of what
you would be able to bring to the project.  </p>
<p>There are many different jobs available, like maintaining the web-site,
coding, documentation, etc. </p>
<p>Experience in these fields would be an advantage, but I feel it is more
important to be willing to give your best and have a strong commitment to open
source development.  Everyone needs to get a start and I'm willing to give that
 chance.</p>
