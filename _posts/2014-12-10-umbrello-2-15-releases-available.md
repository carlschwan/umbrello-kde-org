---
title: 'Umbrello 2.15 releases available'
date: 2014-12-10 00:00:00 
layout: post
---

<p>Umbrello 2.15 releases are available as part of the KDE Applications 14.12 releases. See <a href="/resolvedfeatures.php?14.12">new Umbrello 2.15 features</a> for a list of new features and <a href="/resolvedfeatures.php?14.12">Umbrello 2.15 fixed bugs</a> to see the list of fixed bugs in this releases.</p>
