---
title: 'Umbrello 1.2 Released'
date: 2004-02-03 00:00:00 
layout: post
---

<p>Umbrello 1.2 is the result of a hard year's work by a dedicated team
of developers.  New features include improved code generation in more
languages, undo/redo, a resizeable and zoomable canvas and more
diagram types.  It is part of KDE 3.2.</p>
