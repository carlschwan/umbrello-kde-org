---
title: 'Version 1.0 Released'
date: 2001-09-05 00:00:00 
layout: post
---

<p>I am proud to announce version 1.0 of UML Modeller for KDE 2.2 has been
released. </p>
<p>Check out the ChangeLog for all the changes that have occured.</p>
<p>I have fixed the installing problem people were having (I hope).  Let me know if
it works. </p>
<p>Text is still not transparent, but it is when you print.  So half way there.</p>
<p>Version 1.0 does not keep backward compatability with beta versions.</p>
<p>I will make sure though it will keep compatability from Version 1.0 on.</p>
<p>So download now and give it a try!!!</p>
