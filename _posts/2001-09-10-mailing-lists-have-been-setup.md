---
title: 'Mailing lists have been setup.'
date: 2001-09-10 00:00:00 
layout: post
---

<p>Mailing lists have been setup to allow users to discuss UML Modeller.</p>
<p>Links to subscribe to the mailing lists are located on the contact page.</p>
