---
title: 'ISI Students Merge, Google Summer of Code Begins'
date: 2007-04-23 00:00:00 
layout: post
---

<p>The students from Toulouse's ISI university have <a href="http://www.geeksoc.org/~jr/umbrello/uml-devel/10180.html">merged</a> their work into trunk, including improved undo support.</p>
<p>We now have a student from Google's Summer of Code programme who will work on <a href="http://code.google.com/soc/kde/appinfo.html?csaid=229EB582C8E96E3E">improving</a> <a href="http://sharan.gnuer.org/soc/umbrello/">Entity Relationship diagram support</a>.</p>
