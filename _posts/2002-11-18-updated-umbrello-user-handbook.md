---
title: 'Updated Umbrello User Handbook'
date: 2002-11-18 00:00:00 
layout: post
---

<p>A complete introduction to UML and Umbrello, the handbook is
<a href="handbook/">now online</a>.</p>
<p>Many thanks to Luis for the hard work.</p>
