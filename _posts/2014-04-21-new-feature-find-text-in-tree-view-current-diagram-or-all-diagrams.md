---
title: 'New feature: find text in tree view, current diagram or all diagrams'
date: 2014-04-21 00:00:00 
layout: post
---

<p>Ralf Habacker committed today this new feature. Many thanks for this contribution.</p>
