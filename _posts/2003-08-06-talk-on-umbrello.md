---
title: 'Talk on Umbrello'
date: 2003-08-06 00:00:00 
layout: post
---

<p>Jonathan Riddell gave a talk on Umbrello at UKUUG Linux 2003 Conference in Edinburgh.  <a href="http://jriddell.org/programs/umbrello">Read the slides</a>, <a href="http://www.duffus.org/photos/200307UKUUG/cimg0052">see the photo</a>.</p>

