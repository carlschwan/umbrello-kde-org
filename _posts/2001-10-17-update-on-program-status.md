---
title: 'Update on program status'
date: 2001-10-17 00:00:00 
layout: post
---

<p>I have been getting a lot of E-Mail asking if I have stopped supporting UML Modeller.</p>
<p>Nothing could be further from the truth.  I have University assignments and exams to do, which has force me to suspend programming at the moment.</p>
<p>University finishes on Nov. 22 and expect a release shortly after that with the first version of <b>State</b> diagrams added.</p>
<p>It looks like I will be doing an Honours year next year, this will leave me with plenty of time to continue my work on this program.  So expect plenty of updates.</p>
