---
title: 'Umbrello 1.4.0-beta2 Released'
date: 2005-02-09 00:00:00 
layout: post
---

<p>Umbrello 1.4.0-beta2 has been released for testing along with KDE 3.4.0-beta2.</p>
