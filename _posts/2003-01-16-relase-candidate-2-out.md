---
title: 'Relase Candidate 2 out'
date: 2003-01-16 00:00:00 
layout: post
---

<p>1.1 release candidate 2 is out.  This is a last chance release and
doesn't include such luxuries as a ChangeLog or big announcements.  1.1
should be released early next week.</p>
