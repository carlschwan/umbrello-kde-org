---
title: ' Implementation of "Employ the use of Splines" started'
date: 2013-07-12 00:00:00 
layout: post
---

<p>The implementation of <a href="https://bugs.kde.org/show_bug.cgi?id=107174">Bug 107174</a>, has been started by Andi Fischer. </p>
