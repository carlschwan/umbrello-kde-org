---
title: ' Website update'
date: 2013-07-30 00:00:00 
layout: post
---

<p>The umbrello website has been migrated to the KDE infrastructure and the <a href="http://community.kde.org/KDE.org/Capacity_HOWTO">capacity</a> website framework.</p>
