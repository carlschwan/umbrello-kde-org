---
title: 'New Code Generation in Umbrello 1.2'
date: 2003-09-01 00:00:00 
layout: post
---

<p>A new code generation structure has been imported into Umbrello 1.2
(the KDE CVS version).  This should be a big improvement over the old
code generation.  Currently it is very unstable.  If you want to use
Umbrello 1.2-alpha for real work you should <em>cvs co -D 2003-08-28
kdesdk</em> rather than using the latest CVS version.  Testers of the
new code should <em>rm ~/.kde/share/config/umbrellorc</em> before
running Umbrello.</p>

<p>As of 2003-09-24 Umbrello no longer compiles with KDE/Qt 3.1.</p>

