---
title: "Umbrello Developer Resources"
layout: page 
---
# Umbrello Developer Resources 

## Table of content

1.  [Bugs and feature requests](#bugs-and-feature-requests)
2.  [Changelog](#changelog)
3.  [How to contribute](#how-to-contribute)
4.  [Umbrello continuous build status](#umbrello-continuous-build-status)
5.  [Links](#links)

[](https://umbrello.kde.org/developers.php#links)

## Bugs and feature requests

You may take a look at the wish list, the junior jobs and/or the open bugs:

[](https://umbrello.kde.org/developers.php#links)-   [](https://umbrello.kde.org/developers.php#links)[Umbrello junior jobs](https://bugs.kde.org/buglist.cgi?product=umbrello&query_format=advanced&keywords=junior-jobs&keywords_type=allwords&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id)
-   [Wishlist](https://bugs.kde.org/buglist.cgi?product=umbrello&query_format=advanced&bug_severity=wishlist&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id)
-   [Crash bugs](https://bugs.kde.org/buglist.cgi?product=umbrello&query_format=advanced&bug_severity=crash&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id)
-   [Grave and major bugs](https://bugs.kde.org/buglist.cgi?product=umbrello&query_format=advanced&bug_severity=major&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id)
-   [Normal bugs](https://bugs.kde.org/buglist.cgi?product=umbrello&query_format=advanced&bug_severity=normal&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id)
-   [Minor bugs](https://bugs.kde.org/buglist.cgi?product=umbrello&query_format=advanced&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id)

## Changelog

When bugs get fixed or features are implemented, the related release version is assigned to it. In case you want to see, which bugs has been fixed in which version you may take a look at

-   [Umbrello 2.32 (KDE Applications 20.08) Changelog (not released yet)](https://umbrello.kde.org/changelog.php?20.08)
-   [Umbrello 2.31 (KDE Applications 20.04) Changelog](https://umbrello.kde.org/changelog.php?20.04)
-   [Umbrello 2.30 (KDE Applications 19.12) Changelog](https://umbrello.kde.org/changelog.php?19.12)
-   [Umbrello 2.29 (KDE Applications 19.08) Changelog](https://umbrello.kde.org/changelog.php?19.08)
-   [Umbrello 2.28 (KDE Applications 19.04) Changelog](https://umbrello.kde.org/changelog.php?19.04)
-   [Umbrello 2.27 (KDE Applications 18.12) Changelog](https://umbrello.kde.org/changelog.php?18.12)
-   [Umbrello 2.26 (KDE Applications 18.08) Changelog](https://umbrello.kde.org/changelog.php?18.08)
-   [Umbrello 2.25 (KDE Applications 18.04) Changelog](https://umbrello.kde.org/changelog.php?18.04)
-   [Umbrello 2.24 (KDE Applications 17.12) Changelog](https://umbrello.kde.org/changelog.php?17.12)
-   [Umbrello 2.23 (KDE Applications 17.08) Changelog](https://umbrello.kde.org/changelog.php?17.08)
-   [Umbrello 2.22 (KDE Applications 17.04) Changelog](https://umbrello.kde.org/changelog.php?17.04)
-   [Umbrello 2.21 (KDE Applications 16.12) Changelog](https://umbrello.kde.org/changelog.php?16.12)
-   [Umbrello 2.20 (KDE Applications 16.08) Changelog](https://umbrello.kde.org/changelog.php?16.08)
-   [Umbrello 2.19 (KDE Applications 16.04) Changelog](https://umbrello.kde.org/changelog.php?16.04)
-   [Umbrello 2.18 (KDE Applications 15.12) Changelog](https://umbrello.kde.org/changelog.php?15.12)
-   [Umbrello 2.17 (KDE Applications 15.08) Changelog](https://umbrello.kde.org/changelog.php?15.08)
-   [Umbrello 2.16 (KDE Applications 15.04) Changelog](https://umbrello.kde.org/changelog.php?15.04)
-   [Umbrello 2.15 (KDE Applications 14.12) Changelog](https://umbrello.kde.org/changelog.php?14.12)
-   [Umbrello 2.14 (KDE Applications 4.14) Changelog](https://umbrello.kde.org/changelog.php?4.14)
-   [Umbrello 2.13 (KDE Applications 4.13) Changelog](https://umbrello.kde.org/changelog.php?4.13)
-   [Umbrello 2.12 (KDE Applications 4.12) Changelog](https://umbrello.kde.org/changelog.php?4.12)
-   [Umbrello 2.11 (KDE Applications 4.10) Changelog](https://umbrello.kde.org/changelog.php?4.10)
-   [Complete Changelog](https://umbrello.kde.org/changelog.php)

## How to contribute

If you want to implement a feature already listed in the [wish list](https://bugs.kde.org/buglist.cgi?product=umbrello&query_format=advanced&bug_severity=wishlist&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id) please:

1.  Create a new [bug.kde.org account](https://bugs.kde.org/createaccount.cgi) if not available
2.  Implement the feature and create a merge request for [https://invent.kde.org/kde/umbrello](https://invent.kde.org/kde/umbrello). See [Gitlab project forking workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) for hints how to do.
3.  The related commit message should have the 'FIXED-IN:' and 'BUG:' keyword [1](https://umbrello.kde.org/developers.php#fn1) e.g.
    
    FIXED-IN: 2.29.3 (KDE Release 19.08.3)
    BUG:12345
    
4.  After review has been approved, the reviewer will merge in the patch(es) to the related branch.

In case you are going to fix an annoying bug in umbrello listed in the above mentioned bug list please:

1.  Create a new [bug.kde.org account](https://bugs.kde.org/createaccount.cgi) if not available
2.  Assign the related bug to yourself to indicate that you are working on this bug.
3.  Fix the bug and create a merge request for [https://invent.kde.org/kde/umbrello](https://invent.kde.org/kde/umbrello). See [Gitlab project forking workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) for hints how to do.
4.  The related commit message should have the 'FIXED-IN:' and 'BUG:' keyword[1](https://umbrello.kde.org/developers.php#fn1) e.g.
    
    FIXED-IN: 2.29.3 (KDE Release 19.08.3)
    BUG:12345
    
5.  After review has been approved, the reviewer will merge in the patch(es) to the related branch.

## Umbrello continuous build status

### KDE continuous build (Linux, FreeBSD, Windows MSVC)

-   [Jenkins Umbrello status](https://build.kde.org/job/Applications/job/umbrello/)

### Windows (cross compiled)

-   [Umbrello package (32bit)](https://build.opensuse.org/package/show/windows:mingw:win32/mingw32-umbrello) on Open Build Service.
-   [Umbrello package (64bit)](https://build.opensuse.org/package/show/windows:mingw:win64/mingw64-umbrello) on Open Build Service.

## Links

-   [Umbrello on invent.kde.org](https://invent.kde.org/kde/umbrello)
-   [Umbrello on cgit.kde.org (readonly)](https://cgit.kde.org/umbrello.git)
-   [Umbrello source code checking results](http://www.englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdesdk/umbrello/index.html)
-   [Umbrello contributor status](http://www.ohloh.net/p/umbrello/contributors/summary)

# References

1.This is technical performed by the [FIXED-IN](https://community.kde.org/Policies/Commit_Policy#Special_keywords_in_GIT_and_SVN_log_messages) git keyword.

