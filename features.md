---
title: "Umbrello Features"
layout: page
---
<h1>{{page.title}}</h1>


![](assets/img/UML_diagrams_overview.svg.png)

Most umbrello features and diagram types depends on the public [UML standards](https://en.wikipedia.org/wiki/Unified_Modeling_Language). 

To see what kind of diagram elements are supported, [click here](/features-details.html).      

{% for type in site.data.features-details.types %}
<h2>{{ type.name }}</h2>

<table class="table table-hover">
    <thead class="thead-dark">
        <tr>
            <th>Diagram Types</th>
            <th><a href="http://www.omg.org/spec/UML/1.4/">UML 1.4</a></th>
            <th>Note</th>
            <th><a href="http://www.omg.org/spec/UML/2.0/">UML 2.0</a></th>
            <th>Note</th>
        </tr>
    </thead>
    <tbody>
    {% for table in type.diagram-types %}
            <tr>
                {% if table.icon_url %}
                <td><img src="{{table.icon_url}}" hspace="20">{{ table.name }}</td>
                {%else%}
                <td style="padding-left: 73px;">{{ table.name }}</td>
                {%endif%}
                {%if table.uml14 == "true" or table.uml14 == "partial"%}
                <td><img src="/pics/{{ table.uml14_image}}"></td>
                {%else%}
                <td>{{ table.uml14}}</td>
                {%endif%}
                <td>{{ table.uml20_note }}</td>
                {%if table.uml20 == "true" or table.uml20 == "partial"%}
                <td><img src="/pics/{{ table.uml20_image}}"></td>
                {%else%}
                <td>{{ table.uml20}}</td>
                {%endif%}
                <td>{{ table.uml20_note }}</td>
            </tr>
        {% endfor %}
    </tbody>
</table>
{% endfor %}

<table class="table table-hover">
  <thead class="thead-dark">
      <tr>
        <th>Icon types</th>
        <th><a href="http://www.omg.org/spec/UML/1.4/">UML 1.4</a></th>
        <th>Note</th>
        <th><a href="http://www.omg.org/spec/UML/2.0/">UML 2.0</a></th>
        <th>Note</th>
      </tr>    
    </thead>
  {% for special_one in site.data.features-details.icon-types %}
    <tbody>
      <tr>
        {% if special_one.icon_url %}
        <td><img src="{{site.url}}{{special_one.icon_url}}" hspace="20">{{ special_one.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ special_one.name }}</td>
        {% endif %}
        {%if special_one.uml14 == "true" or special_one.uml14 == "partial"%}
        <td><img src="/pics/{{ special_one.uml14_image }}"></td>
        {%else%}
        <td>{{special_one.uml14}}</td>
        {%endif%}
        <td>{{special_one.uml14_note}}</td>
        {%if special_one.uml20 == "true" or special_one.uml20 == "partial"%}
        <td><img src="/pics/{{ special_one.uml20_image }}"></td>
        {%else%}
        <td>{{special_one.uml20}}</td>
        {%endif%}
        <td>{{special_one.uml20_note}}</td>
      </tr>
      </tbody>
      {%endfor%}
    </table>

<table class="table table-hover">
  <thead class="thead-dark">
      <tr>
        <th>Association types</th>
        <th><a href="http://www.omg.org/spec/UML/1.4/">UML 1.4</a></th>
        <th>Note</th>
        <th><a href="http://www.omg.org/spec/UML/2.0/">UML 2.0</a></th>
        <th>Note</th>
      </tr>    
    </thead>
  {% for special_one in site.data.features-details.association-types %}
    <tbody>
      <tr>
        {% if special_one.icon_url %}
        <td><img src="{{site.url}}{{special_one.icon_url}}" hspace="20">{{ special_one.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ special_one.name }}</td>
        {% endif %}
        {%if special_one.uml14 == "true" or special_one.uml14 == "partial"%}
        <td><img src="/pics/{{ special_one.uml14_image }}"></td>
        {%else%}
        <td>{{special_one.uml14}}</td>
        {%endif%}
        <td>{{special_one.uml14_note}}</td>
        {%if special_one.uml20 == "true" or special_one.uml20 == "partial"%}
        <td><img src="/pics/{{ special_one.uml20_image }}"></td>
        {%else%}
        <td>{{special_one.uml20}}</td>
        {%endif%}
        <td>{{special_one.uml20_note}}</td>
      </tr>
      </tbody>
      {%endfor%}
    </table>

<table class="table table-hover">
  <thead class="thead-dark">
      <tr>
        <th>Graphics primitives</th>
        <th><a href="http://www.omg.org/spec/UML/1.4/">UML 1.4</a></th>
        <th>Note</th>
        <th><a href="http://www.omg.org/spec/UML/2.0/">UML 2.0</a></th>
        <th>Note</th>
      </tr>    
    </thead>
  {% for special_one in site.data.features-details.graphics-primitives %}
    <tbody>
      <tr>
        {% if special_one.icon_url %}
        <td><img src="{{site.url}}{{special_one.icon_url}}" hspace="20">{{ special_one.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ special_one.name }}</td>
        {% endif %}
        {%if special_one.uml14 == "true" or special_one.uml14 == "partial"%}
        <td><img src="/pics/{{ special_one.uml14_image }}"></td>
        {%else%}
        <td>{{special_one.uml14}}</td>
        {%endif%}
        <td>{{special_one.uml14_note}}</td>
        {%if special_one.uml20 == "true" or special_one.uml20 == "partial"%}
        <td><img src="/pics/{{ special_one.uml20_image }}"></td>
        {%else%}
        <td>{{special_one.uml20}}</td>
        {%endif%}
        <td>{{special_one.uml20_note}}</td>
      </tr>
      </tbody>
      {%endfor%}
</table>        

<table class="table table-hover">
  <thead class="thead-dark">
      <tr>
        <th>Languages</th>
        <th>Code Generation</th>
        <th>Notes</th>
        <th>Code Import</th>
        <th>Notes</th>
      </tr>    
    </thead>
  {% for language in site.data.features-details.Languages %}
    <tbody>
      <tr>
        {% if language.icon_url %}
        <td><img src="{{site.url}}{{language.icon_url}}" hspace="20">{{ language.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ language.name }}</td>
        {% endif %}
        {%if language.code_gen == "true" or language.code_gen == "partial"%}
        <td><img src="/pics/{{ language.code_gen_image }}"></td>
        {%else%}
        <td>{{language.code_gen}}</td>
        {%endif%}
        <td>{{language.code_gen_notes}}</td>
        {%if language.code_imp == "true" or language.code_imp == "partial"%}
        <td><img src="/pics/{{ language.code_imp_image }}"></td>
        {%else%}
        <td>{{language.code_imp}}</td>
        {%endif%}
        <td>{{language.code_imp_note}}</td>
      </tr>
      </tbody>
      {%endfor%}
</table>
    
<h2>Supported XMI file formats</h2>
<p>
Umbrello supports XMI 1.2 file import and export (nearly compatible to UML 1.4).
Support for XMI 2.0 is currently in work. See <a href="https://bugs.kde.org/show_bug.cgi?id=115269">Feature Request 115269</a> for more informations.
</p>

<h2>Supported 3rdparty file import formats</h2>
<p>
<ul>
<li>Argo UML</li>
<li>Poseidon for UML</li>
<li>NSUML</li>
<li>Poseidon for UML</li>
<li>UNISYS</li>
</ul>
</p>
