How to setup an umbrello development website on a linux host
===========================================================

1. clone required repositories 

    mkdir <my-root>/websites
    cd <my-root>/websites
    git clone kde:websites/capacity
    git clone kde:websites/umbrello-kde-org

2. install at least 
     apache2
     apache2-mod_php5

   (package names may differ on your distribution, the mentioned
    ones are from opensuse)

3. Create a apache virtual host from the provided template
   with a least the following attributes

    ServerName umbrello-dev.kde.org
    DocumentRoot <my-root>/websites/umbrello-kde-org

    Alias /media <my-root>/websites/capacity
    <Location /media>
        Order allow,deny
        Allow from all
    </Location>

    php_value include_path ".:<my-root>/websites/capacity"

4. (re)start apache2 service

5. add the following line to /etc/hosts

    127.0.0.1 umbrello-dev.kde.org



