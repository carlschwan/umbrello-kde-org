---
title: "Umbrello Features details"
layout: page
---
<h1>{{page.title}}</h1>


![](assets/img/UML_diagrams_overview.svg.png)

Most umbrello features and diagram types depends on the public [UML standards](https://en.wikipedia.org/wiki/Unified_Modeling_Language). To collapse the details click [here]({{site.url}}/features.html)
      
{% for type in site.data.features-details.types %}
  <h2>{{ type.name }}</h2>
  <table class="table table-hover">
    {% for diagram in type.diagram-types %}
    <thead class="thead-dark">
      <tr>
        <th>Diagram type</th>
        <th><a href="http://www.omg.org/spec/UML/1.4/">UML 1.4</a></th>
        <th>Note</th>
        <th><a href="http://www.omg.org/spec/UML/2.0/">UML 2.0</a></th>
        <th>Note</th>
      </tr>
    </thead>
    <tbody>
      <tr class="bg-info">
        {% if diagram.icon_url %}
        <td><img src="{{site.url}}{{diagram.icon_url}}" hspace="20">{{ diagram.name }}</td>
        {% else %}
        <td>{{ diagram.name }}</td>
        {% endif %}
        {%if diagram.uml14 == "true" or diagram.uml14 == "partial"%}
        <td><img src="/pics/{{ diagram.uml14_image }}"></td>
        {%else%}
        <td>{{diagram.uml14}}</td>
        {%endif%}
        <td>{{ diagram.uml20_note }}</td>
        {%if diagram.uml20 == "true" or diagram.uml20 == "partial"%}
        <td><img src="/pics/{{ diagram.uml20_image }}"></td>
        {%else%}
        <td>{{diagram.uml20}}</td>
        {%endif%}
        <td>{{ diagram.uml20_note }}</td>
      </tr>
    </tbody>
    <th>Diagram elements</th>
    {% for detail in diagram.entries %}
      <tr>
        {% if detail.icon_url %}
        <td><img src="{{site.url}}{{detail.icon_url}}" hspace="20">{{ detail.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ detail.name }}</td>
        {% endif %}
        {%if detail.uml14 == "true" or detail.uml14 == "partial"%}
        <td><img src="/pics/{{ detail.uml14_image }}"></td>
        {%else%}
        <td>{{detail.uml14}}</td>
        {%endif%}
        <td>{{ detail.uml14_note }}</td>
        {%if detail.uml20 == "true" or detail.uml20 == "partial"%}
        <td><img src="/pics/{{ detail.uml20_image }}"></td>
        {%else%}
        <td>{{detail.uml20}}</td>
        {%endif%}
        <td>{{ detail.uml20_note }}</td>
      </tr>
    {% endfor %}
  {% endfor %}
  </table>
{% endfor %}

<table class="table table-hover">
  <thead class="thead-dark">
      <tr>
        <th>Icon types</th>
        <th><a href="http://www.omg.org/spec/UML/1.4/">UML 1.4</a></th>
        <th>Note</th>
        <th><a href="http://www.omg.org/spec/UML/2.0/">UML 2.0</a></th>
        <th>Note</th>
      </tr>    
    </thead>
  {% for special_one in site.data.features-details.icon-types %}
    <tbody>
      <tr>
        {% if special_one.icon_url %}
        <td><img src="{{site.url}}{{special_one.icon_url}}" hspace="20">{{ special_one.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ special_one.name }}</td>
        {% endif %}
        {%if special_one.uml14 == "true" or special_one.uml14 == "partial"%}
        <td><img src="/pics/{{ special_one.uml14_image }}"></td>
        {%else%}
        <td>{{special_one.uml14}}</td>
        {%endif%}
        <td>{{special_one.uml14_note}}</td>
        {%if special_one.uml20 == "true" or special_one.uml20 == "partial"%}
        <td><img src="/pics/{{ special_one.uml20_image }}"></td>
        {%else%}
        <td>{{special_one.uml20}}</td>
        {%endif%}
        <td>{{special_one.uml20_note}}</td>
      </tr>
      </tbody>
      {%endfor%}
    </table>

<table class="table table-hover">
  <thead class="thead-dark">
      <tr>
        <th>Association types</th>
        <th><a href="http://www.omg.org/spec/UML/1.4/">UML 1.4</a></th>
        <th>Note</th>
        <th><a href="http://www.omg.org/spec/UML/2.0/">UML 2.0</a></th>
        <th>Note</th>
      </tr>    
    </thead>
  {% for special_one in site.data.features-details.association-types %}
    <tbody>
      <tr>
        {% if special_one.icon_url %}
        <td><img src="{{site.url}}{{special_one.icon_url}}" hspace="20">{{ special_one.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ special_one.name }}</td>
        {% endif %}
        {%if special_one.uml14 == "true" or special_one.uml14 == "partial"%}
        <td><img src="/pics/{{ special_one.uml14_image }}"></td>
        {%else%}
        <td>{{special_one.uml14}}</td>
        {%endif%}
        <td>{{special_one.uml14_note}}</td>
        {%if special_one.uml20 == "true" or special_one.uml20 == "partial"%}
        <td><img src="/pics/{{ special_one.uml20_image }}"></td>
        {%else%}
        <td>{{special_one.uml20}}</td>
        {%endif%}
        <td>{{special_one.uml20_note}}</td>
      </tr>
      </tbody>
      {%endfor%}
    </table>

<table class="table table-hover">
  <thead class="thead-dark">
      <tr>
        <th>Graphics primitives</th>
        <th><a href="http://www.omg.org/spec/UML/1.4/">UML 1.4</a></th>
        <th>Note</th>
        <th><a href="http://www.omg.org/spec/UML/2.0/">UML 2.0</a></th>
        <th>Note</th>
      </tr>    
    </thead>
  {% for special_one in site.data.features-details.graphics-primitives %}
    <tbody>
      <tr>
        {% if special_one.icon_url %}
        <td><img src="{{site.url}}{{special_one.icon_url}}" hspace="20">{{ special_one.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ special_one.name }}</td>
        {% endif %}
        {%if special_one.uml14 == "true" or special_one.uml14 == "partial"%}
        <td><img src="/pics/{{ special_one.uml14_image }}"></td>
        {%else%}
        <td>{{special_one.uml14}}</td>
        {%endif%}
        <td>{{special_one.uml14_note}}</td>
        {%if special_one.uml20 == "true" or special_one.uml20 == "partial"%}
        <td><img src="/pics/{{ special_one.uml20_image }}"></td>
        {%else%}
        <td>{{special_one.uml20}}</td>
        {%endif%}
        <td>{{special_one.uml20_note}}</td>
      </tr>
      </tbody>
      {%endfor%}
</table>

<table class="table table-hover">
  <thead class="thead-dark">
      <tr>
        <th>Languages</th>
        <th>Code Generation</th>
        <th>Notes</th>
        <th>Code Import</th>
        <th>Notes</th>
      </tr>    
    </thead>
  {% for language in site.data.features-details.Languages %}
    <tbody>
      <tr>
        {% if language.icon_url %}
        <td><img src="{{site.url}}{{language.icon_url}}" hspace="20">{{ language.name }}</td>
        {% else %}
        <td style="padding-left: 73px;">{{ language.name }}</td>
        {% endif %}
        {%if language.code_gen == "true" or language.code_gen == "partial"%}
        <td><img src="/pics/{{ language.code_gen_image }}"></td>
        {%else%}
        <td>{{language.code_gen}}</td>
        {%endif%}
        <td>{{language.code_gen_notes}}</td>
        {%if language.code_imp == "true" or language.code_imp == "partial"%}
        <td><img src="/pics/{{ language.code_imp_image }}"></td>
        {%else%}
        <td>{{language.code_imp}}</td>
        {%endif%}
        <td>{{language.code_imp_note}}</td>
      </tr>
      </tbody>
      {%endfor%}
</table>

<h2>Supported XMI file formats</h2>
<p>
Umbrello supports XMI 1.2 file import and export (nearly compatible to UML 1.4).
Support for XMI 2.0 is currently in work. See <a href="https://bugs.kde.org/show_bug.cgi?id=115269">Feature Request 115269</a> for more informations.
</p>

<h2>Supported 3rdparty file import formats</h2>
<p>
<ul>
<li>Argo UML</li>
<li>Poseidon for UML</li>
<li>NSUML</li>
<li>Poseidon for UML</li>
<li>UNISYS</li>
</ul>
</p>
