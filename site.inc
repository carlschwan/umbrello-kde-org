<?php

// promote title to use
$site_title = "Umbrello Project";
$site_logo = "media/images/kde.png";
$site_search = false;
$site_menus = 1;
$templatepath = "chihuahua/";

$site_external = true;
$name = "umbrello.kde.org Webmaster";
$mail = "jr@jriddell.org";
$showedit = false;

$rss_feed_link = "/rss.php";
$rss_feed_title = "Latest Umbrello News";

// some global variables
$kde_current_version = "4.11";

# see https://sysadmin.kde.org/tickets/index.php?page=tickets&act=view&id=TAW-0114
$piwikSiteID = 11;
$piwikEnabled = true;
?>

