<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
#
# redirect to bugs.kde.org and shows a list of open bugs
#
# syntax:
#   openbugs.php       - shows all open bugs
#   openbugs.php?<version> - shows bugs of <version> e.g. 4.8.1 or 4.8
#                            for all releases of the mentioned branch
#
	include(dirname(__FILE__)."/lib.inc");
	$version = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	$url = buglist('openbugs',$version);
	Header("Location: $url");
?>