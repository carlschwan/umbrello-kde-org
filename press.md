---
title: "Press Articles"
layout: page
---
# Press Articles

+ 2012/10/21 [How To Install Umbrello (UML Modeller) On Windows 7](http://sidiq.mercubuana-yogya.ac.id/how-to-install-umbrello-uml-modeller-on-windows-7/)

+ 2012/04/05 [Basics of Umbrello UML Modeller](http://www.zbeanztech.com/blog/basics-umbrello-uml-modeller)
	    
+ 2009/09/17 [Native MacOS Umbrello in KDE 4](http://aufflick.com/blog/2009/02/17/native-macos-umbrello-in-kde4)
	    
+ 2008/29/01 [Penguin-powered UML modeling (The Register)](http://www.theregister.co.uk/2008/01/29/linux_uml_modeling/)
	    
+ 2003/06/01 [Software modelling in (German) linux magazine](http://www.linux-magazin.de/Ausgaben/2003/06/Gut-beschirmt/%28language%29/ger-DE)
	    
+ 2003/04/01 [Jonathan Riddell - BSc Honours Dissertation, Final Report](http://jriddell.org/programs/umbrello/index.pdf)
	    
+ Wikipedia article [](http://en.wikipedia.org/wiki/Umbrello_UML_Modeller)
	    
